from django.urls import path
from todos.views import show_todo_lists, show_detail


urlpatterns = [
    path("", show_todo_lists, name="todo_lists"),
    path("<int:id>", show_detail, name="show_detail"),
]
