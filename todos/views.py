from django.shortcuts import render, get_object_or_404, redirect
from todos.models import ToDoList, ToDoItem


def show_todo_lists(request):
    lists = ToDoList.objects.all()
    context = {
        "todo_list": lists,
    }
    return render(request, "todos/my_lists.html", context)


def show_detail(request, id):
    detail = get_object_or_404(ToDoList, id=id)
    context = {
        "todo_detail": detail
    }
    return render(request, "todos/detail.html", context)
