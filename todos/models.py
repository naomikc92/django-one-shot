from django.db import models


class ToDoList(models.Model):
    name = models.CharField(max_length=200)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class ToDoItem(models.Model):
    task = models.CharField(max_length=100)
    list = models.ForeignKey(
        "ToDoList",
        related_name="items",
        on_delete=models.CASCADE
    )
    due_date = models.DateTimeField(
        null=True,
        blank=True,
    )
    is_completed = models.BooleanField(default=False)
