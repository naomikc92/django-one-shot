# Generated by Django 4.2.1 on 2023-05-31 20:12

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0001_initial"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="ToDo",
            new_name="ToDoList",
        ),
    ]
